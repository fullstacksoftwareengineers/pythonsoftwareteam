# Introduction to Lists
# A list is a collection of items in a particular order. It can include letters, numbers, objects, strings.

bicycle = []
print (bicycle)

# Declare a list
bicycle = ['train', 'truck', 'car']
print bicycle
bicycle.append('ship')
print bicycle

integer_list = [1,2,3,4,5]
print "Printing list of integers: ",integer_list
integer_list.append(6)
print "Updated list of integers: ", integer_list


# How to access values within a list
[0,1,2,3,4,5]  # => index/position
[1,2,3,4,5,6]  # => values in the list
# value 1 in the list has a position of 0
# value 2 in the list has a position of 1
# value 3 in the list has a position of 2
[0      ,   1    ,   2,       3]
['train', 'truck', 'car', 'ship']
# train is at position 0
# truck is at position 1
# car is at position 2

# Accessing values within a list
print "Printing the list called bicycles: ", bicycle
print "The first value within the list is: ",bicycle[0]
print "The second value within the list is: ", bicycle[1]
print bicycle [2]
print bicycle[3]

# Updating the values within a list
print "Before the update on the list ", bicycle
bicycle[0] = 'sports car'
print "After update on the list ",bicycle
bicycle[1] = 'rally'
print "Another update on the list gives us: ", bicycle

# manipulating values in the list
print bicycle[0]
print (bicycle[0].title())#It capitalizes the first letter of each word in the string
bicycle[0] = bicycle[0].title() # Sports Car
print bicycle


# Accessing the last value in a list
[0      ,   1    ,   2,       3]
[-4      ,   -3    ,   -2,       -1]
['train', 'truck', 'car', 'ship']
print bicycle[-1]
print bicycle[-2]
print bicycle[-3]
print bicycle[-4]

# Try this out youself
# I need to try this out myself
# This is what I planned to try out
# TODO I need to work on this portion of my code later

'''
1. Names: Store the names of a few of your friends in a list called names, print each person's name by
accessing each element in the list, one at a time
'''
# Adding and removing elements within a list
# add values dynamically to the bicycle list
bicycle.append('railway')
print bicycle

# Initializing a new list
motorcycles = []
motorcycles.append('honda')
motorcycles.append('yamaha')
motorcycles.append('suzuki')

print (motorcycles)

# Inserting elements into a list at a specified position
[0,         1,         2]
['honda', 'yamaha', 'suzuki']
# To put a value in the list between honda and yamaha
motorcycles.insert(1, 'mitsubishi')
print motorcycles
# ['honda', 'mitsubishi', 'yamaha', 'suzuki']
motorcycles.insert(2,'toyota')
print motorcycles
#['honda', 'mitsubishi', 'toyota', 'yamaha', 'suzuki']

# Removing elements from a list
# Deleting mitsubishi at position 1 in the motorcycles list
del(motorcycles[1])
print motorcycles
del(motorcycles[0])
print motorcycles

# Using the pop function(to remove values from the end of the list)
motorcycle = motorcycles.pop()
print motorcycle
print motorcycles

# Popping items from any position in a list
# You can actually pop() to remove an item in a list at any position by including the index of the item
value = motorcycles.pop(0)
print value
print motorcycles

# Remove an item by value from a list
# There are times when you don't know the position of the value you remove
motorcycles.append('honda')
motorcycles.append('mitsubishi')
motorcycles.append('suzuki')
print motorcycles
print motorcycles.remove('yamaha')
print motorcycles
too_expensive = 'suzuki'
motorcycles.remove(too_expensive) # motorcycles.remove('suzuki)
print motorcycles


# Organizing a list
# Sort a list permanently with the sort() method
cars = ['toyota', 'honda', 'bmw']
cars.sort()
print cars

















