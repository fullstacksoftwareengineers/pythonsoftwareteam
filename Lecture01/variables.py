# variable var is a string
var = "Hello Python Course!"

print var

num1 = 55

print "This is the value of number1: ", num1

num2 = 5

print num1 + num2

# Change the value of num1

num1 = "Change"

print "This is the value of number1: \t" + num1

# In python 3

#print(num1 + num2)

# Looking at Strings
str1 = "This is a string"
str2 = 'This is also a string'

print "This is string1: ", str1
print "This is string2: ", str2

# Concatenating strings
print str1, str2 # One way of Concatenating strings
print str1 + " " + str2 # Another way of Concatenating strings

str3 = "Today is a very nice day I am glad"

print str3

# To capitalize all first letters of every word within the string
print str3.title()
print str3.upper()
str3 = str3.upper()
print str3

str4 = "necklace"
print "I really love the" + " " + str4 + " which I bought yesterday"
print "I really love the" + " " + str4.upper() + " which I bought yesterday"

# Working with whitespaces and tabs
spaceVar = " python is fun "
if(spaceVar == "python is fun"):
    print True
else:
    print False

if spaceVar.__contains__("python"):
    print True

print spaceVar
spaceVar = spaceVar.strip()
print spaceVar
if(spaceVar == "python is fun"):
    print True
else:
    print False

spaceVar2 = "python is fun  b  "
print spaceVar2
spaceVar2 = spaceVar2.rstrip()
print spaceVar2
if(spaceVar2 == "python is fun"):
    print True
else:
    print False
first_string = "This language helps me a lot"
second_string = "THIS Language helps me a lot"

if first_string == second_string:
    print "\nThey are equal"
else:
    print "\nThey are not equal"

# Ternary operator-  a if condition else b
print ("True" if first_string == second_string else "False")

#Arithmetic operations in python
a = 5
b = 7
c = a + b
print c

# say you have a = 5, b = 6, How do I swap these values without using a swap or temp variable.







